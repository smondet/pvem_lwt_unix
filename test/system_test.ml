#! /bin/sh

PACKAGES=pvem,lwt.unix,lwt.preemptive

MD5=`md5sum $0  | cut -d ' ' -f 1`
BASE=/tmp/ocaml_script_$MD5/
mkdir -p $BASE

ML_FILE=${BASE}/source.ml
EXEC=${BASE}/`basename $0`

if test -f $BASE
then
  $EXEC $*
  RETURN_CODE=$?
else

  SKIP=`awk '/^__OCAML_FOLLOWS__/ { print NR + 1; exit 0; }' $0`
  echo "#$SKIP \"$0\"" > $ML_FILE
  tail -n +$SKIP $0 >> $ML_FILE

  ocamlfind ocamlopt  -I _build/ pvem_lwt_unix.cmxa -thread -package $PACKAGES \
    -linkpkg -o $EXEC $ML_FILE \
    && $EXEC $*
  RETURN_CODE=$?
fi
exit $RETURN_CODE

__OCAML_FOLLOWS__

(**************************************************************************)
(*  Copyright (c) 2012, 2013,                                             *)
(*                           Sebastien Mondet <seb@mondet.org>,           *)
(*                           Ashish Agarwal <agarwal1975@gmail.com>.      *)
(*                                                                        *)
(*  Permission to use, copy, modify, and/or distribute this software for  *)
(*  any purpose with or without fee is hereby granted, provided that the  *)
(*  above  copyright notice  and this  permission notice  appear  in all  *)
(*  copies.                                                               *)
(*                                                                        *)
(*  THE  SOFTWARE IS  PROVIDED  "AS  IS" AND  THE  AUTHOR DISCLAIMS  ALL  *)
(*  WARRANTIES  WITH  REGARD  TO  THIS SOFTWARE  INCLUDING  ALL  IMPLIED  *)
(*  WARRANTIES  OF MERCHANTABILITY AND  FITNESS. IN  NO EVENT  SHALL THE  *)
(*  AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL  *)
(*  DAMAGES OR ANY  DAMAGES WHATSOEVER RESULTING FROM LOSS  OF USE, DATA  *)
(*  OR PROFITS,  WHETHER IN AN  ACTION OF CONTRACT, NEGLIGENCE  OR OTHER  *)
(*  TORTIOUS ACTION,  ARISING OUT  OF OR IN  CONNECTION WITH THE  USE OR  *)
(*  PERFORMANCE OF THIS SOFTWARE.                                         *)
(**************************************************************************)

open Printf
open Pvem_lwt_unix
open Deferred_result
module List = struct

  include ListLabels

  let concat_map l ~f =
    let rec aux acc = function
    | [] -> List.rev acc
    | hd :: tl -> aux (rev_append (f hd) acc) tl
    in
    aux [] l
end
module String = StringLabels

let say fmt =
  ksprintf (fun s -> eprintf "%s\n%!" s) fmt

let cmdf fmt =
  ksprintf (fun s ->
    say "CMD: %S" s;
    System.Shell.do_or_fail s) fmt

let fail_test fmt =
  ksprintf (fun s -> fail (`Failed_test s)) fmt

let test_make_directory () =
  let tmp = Filename.temp_file "sys_test_make_directory" "_dir" in
  ksprintf System.Shell.do_or_fail "rm -fr %s" tmp
  >>= fun () ->
  System.ensure_directory_path ~perm:0o777 tmp
  >>= fun () ->
  begin
    System.ensure_directory_path "/please_dont_run_tests_as_root"
    >>< begin function
    | `Ok () -> say "ERROR: This should have failed!!"; return ()
    | `Error (`System (`Make_directory _, `Wrong_access_rights _)) -> return ()
    | `Error e -> say "ERROR: Got wrong error"; fail e
    end
  end
  >>= fun () ->
  begin
    System.make_new_directory tmp
    >>< begin function
    | `Ok () -> fail_test "This should have failed make_directory ~parents:false"
    | `Error (`System (`Make_directory _, `Already_exists)) -> return ()
    | `Error e -> fail e
    end
  end
  >>= fun () ->
  let path = Filename.concat tmp "some/very/long/path" in
  System.ensure_directory_path path
  >>= fun () ->
  ksprintf System.Shell.do_or_fail "find %s -type d" tmp
  >>= fun () ->
  say "test_make_directory: OK";
  return ()

let test_file_info () =
  let tmp = Filename.temp_file "sys_test_file_info" "_dir" in
  ksprintf System.Shell.do_or_fail "rm -f %s && mkdir -p %s && cd %s && ln -s /tmp symlink_to_dir" tmp tmp tmp
  >>= fun () ->
  ksprintf System.Shell.do_or_fail "cd %s && ln -s /etc/passwd symlink_to_file" tmp
  >>= fun () ->
  let check ?follow_symlink matches path =
    System.file_info ?follow_symlink path
    >>= begin function
    | o when matches o -> return ()
    | e -> fail (`Wrong_file_info (path, e))
    end
  in
  check ((=) `Directory) "/" >>= fun () ->
  check (function `Regular_file _ -> true | _ -> false) "/etc/passwd" >>= fun () ->
  ksprintf (check ((=) (`Symlink "/etc/passwd"))) "%s/symlink_to_file" tmp
  >>= fun () ->
  ksprintf (check ((=) (`Symlink "/tmp"))) "%s/symlink_to_dir" tmp
  >>= fun () ->
  check ((=) `Absent) "/sldkfjslakjfdlksj"
  >>= fun () ->

  ksprintf System.Shell.do_or_fail "ls -l %s " tmp
  >>= fun () ->
  say "test_file_info: OK";
  return ()

let is_present ?(and_matches=(fun _ -> true)) path =
  System.file_info path
  >>= begin function
  | `Absent -> fail (`Wrong_file_info (path, `Absent))
  | any when and_matches any -> return ()
  | any_other -> fail (`Wrong_file_info (path, any_other))
  end

let is_absent path =
  System.file_info path
  >>= begin function
  | `Absent -> return ()
  | e -> fail (`Wrong_file_info (path, e))
  end

let random_tree path max_number_creations =
  let max_number_creations = ref max_number_creations in
  let rec random_tree_aux path =
    let random_ints =
      Array.(to_list (init (Random.int 20 + 20) (fun e -> e))) in
    ListLabels.fold_left ~init:(return ()) random_ints
      ~f:(fun prev_m id ->
          prev_m >>= fun () ->
          if !max_number_creations <= 0 then return ()
          else begin
            let path n =
              ksprintf (Filename.concat path) "random_%s_%d" n id in
            decr max_number_creations;
            begin match Random.int 3 with
            | 0 ->
              let p = path "dir" in
              System.ensure_directory_path p
              >>= fun () ->
              random_tree_aux p
            | 1 ->
              let size = Random.int 1_000_000 in
              let content = String.create size in
              let p = ksprintf path "file_of_size_%d" size in
              IO.write_file p ~content
            | _ ->
              let p = path "symlink" in
              System.make_symlink ~target:"/tmp/bouh" ~link_path:p
            end
          end)
  in
  random_tree_aux path

let test_remove style =
  let in_dir =
    match style with
    | `Relative -> "test_flow_system_remove"
    | `Absolute -> "/tmp/test_flow_system_remove" in
  cmdf "rm -fr %s" in_dir
  >>= fun () ->
  System.ensure_directory_path in_dir
  >>= fun () ->
  let test_regular = Filename.concat in_dir "reg_file" in
  IO.write_file test_regular ~content:"come content"
  >>= fun () ->
  System.file_info test_regular
  >>= begin function
  | `Regular_file l when l > 2 -> return ()
  | e -> fail (`Wrong_file_info (test_regular, e))
  end
  >>= fun () ->
  System.remove test_regular
  >>= fun () ->
  say "Removed %s" test_regular;
  is_absent test_regular
  >>= fun () ->
  let test_empty_dir = Filename.concat in_dir "empty_dir" in
  System.ensure_directory_path test_empty_dir >>= fun () ->
  is_present test_empty_dir >>= fun () ->
  System.remove test_empty_dir >>= fun () ->
  is_absent test_empty_dir >>= fun () ->
  say "Removed %s" test_empty_dir;
  let test_non_empty_dir = Filename.concat in_dir "non_empty_dir" in
  System.ensure_directory_path test_non_empty_dir >>= fun () ->
  random_tree test_non_empty_dir 100
  >>= fun () ->
  cmdf "find %s | wc -l" test_non_empty_dir
  >>= fun () ->
  System.remove test_non_empty_dir >>= fun () ->
  is_absent test_non_empty_dir >>= fun () ->

  let test_symlink = Filename.concat in_dir "test_symlink" in
  System.make_symlink ~target:"/tmp/bouh" ~link_path:test_symlink
  >>= fun () ->
  is_present ~and_matches:((=) (`Symlink "/tmp/bouh")) test_symlink >>= fun () ->
  System.remove test_symlink >>= fun () ->
  is_absent test_symlink >>= fun () ->
  say "Removed: %s" test_symlink;

  System.remove in_dir >>= fun () ->
  is_absent in_dir >>= fun () ->

  say "test_remove: OK";
  return ()


let check_error_file_exists name expected_path =
  begin function
  | `Ok () -> fail_test "%s: default 'if_exists' should be `Fail" name
  | `Error (`System (_, (`File_exists p))) when p = expected_path -> return ()
  | `Error (`System (_, `IO (`File_exists p))) when p = expected_path -> return ()
  | `Error e -> fail e
  end

let test_copy style_in style_out =
  let out_dir =
    match style_out with
    | `Relative -> "test_flow_system_copy_target"
    | `Absolute -> "/tmp/test_flow_system_copy_target" in
  let in_dir =
    match style_in with
    | `Relative -> "test_flow_system_copy"
    | `Absolute -> "/tmp/test_flow_system_copy" in
  say "test_copy %s/… → %s/…" in_dir out_dir;
  cmdf "rm -fr %s" in_dir >>= fun () ->
  cmdf "rm -fr %s" out_dir >>= fun () ->
  System.ensure_directory_path in_dir >>= fun () ->
  System.ensure_directory_path out_dir >>= fun () ->

  (******************)
  (* Symbolic links *)
  (******************)

  let test_symlink = Filename.concat in_dir "test_symlink" in
  System.make_symlink ~target:"/tmp/bouh" ~link_path:test_symlink
  >>= fun () ->

  say "`Redo symlinks %s into %s" test_symlink out_dir;
  System.copy ~symlinks:`Redo ~src:test_symlink (`Into out_dir)
  >>= fun () ->
  let expected_path = Filename.concat out_dir "test_symlink" in
  is_present ~and_matches:((=) (`Symlink "/tmp/bouh")) expected_path
  >>= fun () ->

  System.copy ~symlinks:`Redo ~src:test_symlink (`Into out_dir)
  >>< check_error_file_exists "copy-redo-symlink" expected_path
  >>= fun () ->

  System.copy ~symlinks:`Redo ~src:test_symlink ~if_exists:`Overwrite (`Into out_dir)
  >>= fun () ->

  say "redo symlink %s as %s" test_symlink out_dir;
  let dst = Filename.concat out_dir "test_symlink_new" in
  System.copy ~symlinks:`Redo ~src:test_symlink (`Onto dst)
  >>= fun () ->
  is_present ~and_matches:((=) (`Symlink "/tmp/bouh")) dst
  >>= fun () ->
  System.remove dst (* we remove this one to be able to compare with
                       file_tree at the end *)
  >>= fun () ->

  (**************************************)
  (* Copying files (with various sizes) *)
  (**************************************)
  let test_copy_file size =
    let test_reg_file = Filename.concat in_dir (sprintf "reg_file_%d" size) in
    let content = String.make size 'B' in
    IO.write_file test_reg_file ~content
    >>= fun () ->
    System.copy ~src:test_reg_file (`Into out_dir)
    >>= fun () ->
    let expected_path = Filename.(concat out_dir (basename test_reg_file)) in
    is_present ~and_matches:((=) (`Regular_file size)) expected_path
    >>= fun () ->
    IO.read_file expected_path
    >>= fun content_got ->
    begin
      if content_got = content
      then return ()
      else
        fail_test "contents of %s and %s are different"
          test_reg_file expected_path
    end
    >>= fun () ->
    System.copy ~src:test_reg_file (`Into out_dir)
    >>< check_error_file_exists "copy-file" expected_path
    >>= fun () ->
    (* This one should work: *)
    System.copy ~if_exists:`Overwrite ~src:test_reg_file (`Into out_dir)
  in
  test_copy_file 200 >>= fun () ->
  test_copy_file 200_000 >>= fun () ->

  (**************************************)
  (* Copying full trees of files        *)
  (**************************************)
  let subtree_path = Filename.concat in_dir "random_tree" in
  System.ensure_directory_path subtree_path
  >>= fun () ->
  random_tree subtree_path 20
  >>= fun () ->

  System.copy ~symlinks:`Redo ~src:subtree_path (`Into out_dir)
  >>= fun () ->

  System.copy ~src:subtree_path (`Into out_dir)
  >>< check_error_file_exists "copy-whole-random-tree"
    Filename.(concat out_dir "random_tree")
  >>= fun () ->

  (* The file_tree comparison mostly shows that the copy worked and that
     the failing one did not add anything. *)
  let compare_file_trees in_dir out_dir fmt =
    System.file_tree ~follow_symlinks:false in_dir
    >>= fun in_tree ->
    System.file_tree ~follow_symlinks:false out_dir
    >>= fun out_tree ->
    begin match in_tree, out_tree with
    | `Node (inname, lin), `Node (outname, lout)
      when inname = Filename.basename in_dir
      && outname = Filename.basename out_dir
        && lin = lout ->
      return ()
    | _ ->
      (* say "in_tree: %s" (Sexp.to_string_hum (System.sexp_of_file_tree in_tree)); *)
      (* say "out_tree: %s" (Sexp.to_string_hum (System.sexp_of_file_tree out_tree)); *)
      ksprintf (fun s -> fail_test "in_tree <> out_tree %s" s) fmt
    end
  in
  compare_file_trees in_dir out_dir "after copy + copy-failure"
  >>= fun () ->

  (* `Overwrite should succeed and leave exactly the same result. *)
  System.copy ~if_exists:`Overwrite ~symlinks:`Redo ~src:subtree_path (`Into out_dir)
  >>= fun () ->
  compare_file_trees in_dir out_dir "after overwriting"
  >>= fun () ->

  (* `Update should add/overwrite files but not remove *)
  System.file_tree ~follow_symlinks:false out_dir
  >>= fun init_dir ->
  System.remove subtree_path  >>= fun () ->
  is_absent subtree_path >>= fun () ->
  let another_directory = Filename.concat subtree_path "another_one" in
  System.ensure_directory_path another_directory  >>= fun () ->
  let one_file = Filename.concat another_directory "one_file" in
  IO.write_file one_file ~content:"AAAA"
  >>= fun () ->
  let another_file = Filename.concat subtree_path "file" in
  IO.write_file another_file ~content:"BBBB" >>= fun () ->
  (* We now have:
     subtree_path/another_one/one_file
     subtree_path/file *)
  System.file_tree ~follow_symlinks:false out_dir
  >>= fun out_tree_before ->
  System.copy ~if_exists:`Update ~symlinks:`Redo ~src:subtree_path (`Into out_dir)
  >>= fun () ->
  System.file_tree ~follow_symlinks:false out_dir
  >>= fun out_tree ->
  let rec path_list path tree =
    match tree with
    | `Leaf (f, _) -> [Filename.concat path f]
    | `Node (n, l) ->
      let now = Filename.concat path n in
      now :: List.concat_map l ~f:(fun sub -> path_list now sub)
  in
  begin match out_tree_before, out_tree with
  | `Node (dir, content_before), `Node (same, content_after) when dir = same ->
    let paths_before = path_list "." out_tree_before in
    let paths_after = path_list "." out_tree in
    let check f =
      try ignore (ListLabels.find paths_after ~f:(fun p -> Filename.basename p = f));
        return ()
      with
      | _ -> fail_test "paths_after does not have %s" f
    in
    check  "another_one" >>= fun () ->
    check  "one_file" >>= fun () ->
    check  "file" >>= fun () ->
    let included =
      ListLabels.for_all paths_before ~f:(fun p -> List.mem ~set:paths_after p) in
    if included then return ()
    else (
      say "old_paths:\n%s" (String.concat ~sep:"\n" paths_before);
      say "new_paths:\n%s" (String.concat ~sep:"\n" paths_after);
      fail_test "paths_before not-included-in paths_after"
    )
  | _ ->
    (* say "out_tree_before: %s" (Sexp.to_string_hum (System.sexp_of_file_tree out_tree_before)); *)
    (* say "out_tree: %s" (Sexp.to_string_hum (System.sexp_of_file_tree out_tree)); *)
    fail_test "out_tree_before <> out_tree (copy `Update)"
  end
  >>= fun () ->

  System.remove in_dir >>= fun () ->
  System.remove out_dir >>= fun () ->
  say "test_copy: OK";
  return ()

let test_move style_in style_out =
  (*
    Note: to test the `Move` function in the [`Must_copy] case one has to
    launch this test from another partition than the one containing "/tmp".
  *)
  let out_dir =
    match style_out with
    | `Relative -> "test_flow_system_move_target"
    | `Absolute -> "/tmp/test_flow_system_move_target" in
  let in_dir =
    match style_in with
    | `Relative -> "test_flow_system_move"
    | `Absolute -> "/tmp/test_flow_system_move" in
  say "test_move %s/… → %s/…" in_dir out_dir;
  cmdf "rm -fr %s" in_dir >>= fun () ->
  cmdf "rm -fr %s" out_dir >>= fun () ->
  System.ensure_directory_path in_dir >>= fun () ->
  System.ensure_directory_path out_dir >>= fun () ->

  let test_symlink = Filename.concat in_dir "test_symlink" in
  System.make_symlink ~target:"/tmp/bouh" ~link_path:test_symlink
  >>= fun () ->
  System.move ~symlinks:`Redo ~src:test_symlink (`Into out_dir)
  >>= fun () ->
  let expected_path = Filename.concat out_dir "test_symlink" in
  is_present ~and_matches:((=) (`Symlink "/tmp/bouh")) expected_path
  >>= fun () ->
  is_absent test_symlink >>= fun () ->

  let subtree_path = Filename.concat in_dir "random_tree" in
  System.ensure_directory_path subtree_path >>= fun () ->
  random_tree subtree_path 20 >>= fun () ->
  System.file_tree ~follow_symlinks:false subtree_path
  >>= fun src_tree ->

  let new_tree = Filename.concat out_dir "random_tree_moved" in
  System.move ~symlinks:`Redo ~src:subtree_path (`Onto new_tree)
  >>= fun () ->
  System.file_tree ~follow_symlinks:false new_tree
  >>= fun dst_tree ->
  is_absent subtree_path
  >>= fun () ->

  begin match src_tree, dst_tree with
  | `Node (inname, lin), `Node (outname, lout)
    when inname = "random_tree" && outname = "random_tree_moved"
                && lin = lout ->
    return ()
  | _ ->
    (* say "src_tree: %s" (Sexp.to_string_hum (System.sexp_of_file_tree src_tree)); *)
    (* say "dst_tree: %s" (Sexp.to_string_hum (System.sexp_of_file_tree dst_tree)); *)
    fail_test "src_tree <> dst_tree"
  end
  >>= fun () ->


  System.ensure_directory_path (Filename.concat in_dir "somedir")
  >>= fun () ->
  System.ensure_directory_path (Filename.concat out_dir "somedir")
  >>= fun () ->
  System.move (Filename.concat in_dir "somedir") (`Into out_dir)
  >>< check_error_file_exists "move should fail if exists"
    (Filename.concat out_dir "somedir")
  >>= fun () ->
  System.move ~if_exists:`Update ~src:(Filename.concat in_dir "somedir") (`Into out_dir)
  >>= fun () ->

  System.remove in_dir >>= fun () ->
  System.remove out_dir >>= fun () ->
  say "test_move: OK";
  return ()

let test_shell () =
  let silent fmt =
    ksprintf (fun s ->
      ksprintf System.Shell.do_or_fail "( %s ) > /dev/null 2>&1" s) fmt in
  silent "ls /tmp"
  >>= fun () ->

  let bind_on_error m f =
    m >>< function `Ok o -> return o | `Error e -> f e in

  bind_on_error (silent "ls /some_big_path")
    begin function
    | `Shell (_, `Exited 2) | `Shell (_, `Exited 1) -> return ()
    | e -> fail e
    end
  >>= fun () ->

  bind_on_error (silent "kill $$")
    begin function
    | `Shell (_, `Signaled s) when s = Sys.sigterm -> return ()
    | e -> fail e
    end
  >>= fun () ->

  bind_on_error (silent "kill -9 $$")
    begin function
    | `Shell (_, `Signaled s) when s = Sys.sigkill -> return ()
    | e -> fail e
    end
  >>= fun () ->


  let check_output ~ok fmt =
    ksprintf (fun s ->
      System.Shell.execute s
      >>= begin function
      | (sin, sout, ex) when ok sin sout ex -> return ()
      | (sin, sout, ex) ->
        fail_test "output of '%s':\n%S\n%S\n%S" s sin sout
          (System.Shell.status_to_string ex)
      end
    ) fmt  in

  check_output "ls /"
    ~ok:(fun sin sout ex ->
      ex = `Exited 0 && sout = "" && String.length sin > 10)
  >>= fun () ->

  check_output "ls /some_big_path"
    ~ok:(fun sin sout ex -> ex = `Exited 2 || ex = `Exited 1)
  >>= fun () ->

  check_output "kill -9 $$" ~ok:(fun _ _ ex -> ex = `Signaled Sys.sigkill)
  >>= fun () ->

  check_output "echo 'bouh'; exit 2"
    ~ok:(fun sin sout ex ->
      ex = `Exited 2
      && sin = "bouh\n")
  >>= fun () ->

  say "test_shell: OK";
  return ()

let main () =
  say "sys_test: GO!";
  let (>>=) m f =
    m >>= (fun o ->
      say "=======================================";
      f o) in
  test_make_directory ()
  >>= fun () ->
  test_file_info ()
  >>= fun () ->
  test_remove `Relative >>= fun () ->
  test_remove `Absolute >>= fun () ->
  test_copy `Relative `Relative  >>= fun () ->
  test_copy `Absolute `Absolute  >>= fun () ->
  test_copy `Absolute `Relative  >>= fun () ->
  test_copy `Relative `Absolute  >>= fun () ->
  test_move `Relative `Relative  >>= fun () ->
  test_move `Absolute `Absolute  >>= fun () ->
  test_move `Absolute `Relative  >>= fun () ->
  test_move `Relative `Absolute  >>= fun () ->
  test_shell () >>= fun () ->
  say "sys_test: Successful End";
  return ()

let () =
  let errf f e fmt =
    ksprintf (fun s ->
      eprintf "XXXX %s:\n%s%!" s (f e);
      exit 1
    ) fmt
  in
  match Lwt_main.run (main ()) with
  | `Ok () -> exit 0
  | `Error (`IO _ as e) -> errf IO.error_to_string e "I/O Error"
  | `Error (`System _ as e) | `Error (`Shell _ as e) ->
    errf System.error_to_string e "System Error"
  | `Error (`Wrong_file_info (p, info)) -> 
    errf System.file_info_to_string info "Wrong file info for %S" p
  | `Error (`Failed_test s) ->
    errf (fun e -> e) s "Test FAILED"

