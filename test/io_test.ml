#! /bin/sh

PACKAGES=pvem,lwt.unix,lwt.preemptive

MD5=`md5sum $0  | cut -d ' ' -f 1`
BASE=/tmp/ocaml_script_$MD5/
mkdir -p $BASE

ML_FILE=${BASE}/source.ml
EXEC=${BASE}/`basename $0`

if test -f $BASE
then
  $EXEC $*
  RETURN_CODE=$?
else

  SKIP=`awk '/^__OCAML_FOLLOWS__/ { print NR + 1; exit 0; }' $0`
  echo "#$SKIP \"$0\"" > $ML_FILE
  tail -n +$SKIP $0 >> $ML_FILE

  ocamlfind ocamlopt  -I _build/ pvem_lwt_unix.cmxa -thread -package $PACKAGES \
   -linkpkg -o $EXEC $ML_FILE \
    && $EXEC $*
  RETURN_CODE=$?
fi
exit $RETURN_CODE

__OCAML_FOLLOWS__

(**************************************************************************)
(*  Copyright (c) 2012, 2013,                                             *)
(*                           Sebastien Mondet <seb@mondet.org>,           *)
(*                           Ashish Agarwal <agarwal1975@gmail.com>.      *)
(*                                                                        *)
(*  Permission to use, copy, modify, and/or distribute this software for  *)
(*  any purpose with or without fee is hereby granted, provided that the  *)
(*  above  copyright notice  and this  permission notice  appear  in all  *)
(*  copies.                                                               *)
(*                                                                        *)
(*  THE  SOFTWARE IS  PROVIDED  "AS  IS" AND  THE  AUTHOR DISCLAIMS  ALL  *)
(*  WARRANTIES  WITH  REGARD  TO  THIS SOFTWARE  INCLUDING  ALL  IMPLIED  *)
(*  WARRANTIES  OF MERCHANTABILITY AND  FITNESS. IN  NO EVENT  SHALL THE  *)
(*  AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL  *)
(*  DAMAGES OR ANY  DAMAGES WHATSOEVER RESULTING FROM LOSS  OF USE, DATA  *)
(*  OR PROFITS,  WHETHER IN AN  ACTION OF CONTRACT, NEGLIGENCE  OR OTHER  *)
(*  TORTIOUS ACTION,  ARISING OUT  OF OR IN  CONNECTION WITH THE  USE OR  *)
(*  PERFORMANCE OF THIS SOFTWARE.                                         *)
(**************************************************************************)

open Pvem_lwt_unix
open Deferred_result
open Printf

let say fmt =
  ksprintf (fun s -> eprintf "%s\n%!" s) fmt

let copy () =
  let tmp = Filename.temp_file "io_test_copy" ".bin" in
  IO.write_file tmp ~content:"foo!"
  >>= fun () ->
  IO.with_in_channel (`File tmp) ~buffer_size:42 ~f:(fun i ->
    IO.read i
    >>= fun content ->
    IO.with_out_channel (`Stdout) ~f:(fun o ->
      ksprintf (IO.write o) "Content of %s is %S\n" tmp content))


let fail_test fmt =
  ksprintf (fun s -> fail (`Test_failed s)) fmt

let fail_test_if cond fmt =
  ksprintf (fun s ->
    if cond then fail (`Test_failed s) else return ()) fmt

let test_with_out_channel () =
  let tmp = Filename.temp_file "io_test_with_out_channel" ".bin" in
  IO.write_file tmp ~content:"A"
  >>= fun () ->
  IO.with_out_channel (`Append_to_file tmp) ~f:(fun o ->
    IO.write o "B")
  >>= fun () ->
  IO.read_file tmp
  >>= fun content ->
  fail_test_if (content <> "AB") "append_to_file"
  >>= fun () ->
  begin
    IO.with_out_channel (`Create_file tmp) ~f:(fun o ->
      IO.write o "B")
    >>< begin function
    | `Ok () -> fail_test "test_with_out_channel.create: could write in %s" tmp
    |`Error (`IO (`File_exists p)) -> return ()
    (* |`Error (`IO (`Exn e)) -> *)
      (* eprintf "io_exn: %s\n%!" Exn.(to_string e); *)
      (* fail (`Io_exn e) *)
    |`Error e -> fail e
    end
  end
  >>= fun () ->
  System.remove tmp >>< fun _ ->

  IO.with_out_channel (`Create_file tmp) ~f:(fun o ->
    IO.write o "AB")
  >>= fun () ->

  IO.read_file tmp >>= fun content ->
  fail_test_if (content <> "AB") "effectively create"
  >>= fun () ->

  IO.with_out_channel (`Overwrite_file tmp) ~f:(fun o ->
    IO.write o "CD")
  >>= fun () ->

  IO.read_file tmp >>= fun content ->
  fail_test_if (content <> "CD") "overwrite_file"
  >>= fun () ->

  say "test_with_out_channel: OK";
  return ()


let main () =
  copy ()
  >>= fun () ->
  test_with_out_channel ()

let () =
  match Lwt_main.run (main ()) with
  | `Ok () -> ()
  | `Error (`IO _ as e) ->
    eprintf "End with I/O error:\n%s\n%!" (IO.error_to_string e);
    exit 1
  | `Error (`Test_failed s) ->
    eprintf "Test FAILED::\n%s\n%!" s;
    exit 1
