Pvem\_lwt\_unix
===============

This library provides an exception-less interface to *unix-ish*
operating systems. As usual, this means all flavors of POSIX-compliant
systems, like Linux, *BSD, Mac OSX, and partially MS Windows.

Modules
-------

The module `Pvem_lwt_unix` contains the following sub-modules:

- **Deferred_result** which is `Pvem.With_deferred(Lwt)`, i.e., basic
  functions for manipulating `('ok, 'error) Pvem.Result.t Lwt.t`,
- **IO** which implements the `IO` module type,
- **System** implements the `SYSTEM` module type,
- **Deferred_list** implement the `DEFERRED_LIST` module type;
sequential and concurrent loops using the `List.t` type.
- **Light** implements the `LIGHT` signature, a very simple traffic light for
concurrent programs (experimental work in progress).

Tests
-----

You may run the tests, this way:

     ./test/io_test.ml
     ./test/system_test.ml

Note that `system_test.ml` expects to be run as non-root (because it
uses the fact that some paths are not writable to check access-rights
errors).
